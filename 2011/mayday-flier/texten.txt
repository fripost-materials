Fripost – demokratisk e-post

Fripost bygger demokratiskt kontrollerad e-post för sina medlemmar.

På Google, Facebook eller Twitter kontrolleras tekniken av stora privata
företag. De har gjort till affärsidé att tjulyssna på sina användare och tjänar
pengar på att bygga stora och detaljerade databaser över användarnas privatliv.

Bara ett fåtal har kunskap och resurser nog att själva bygga egna system för att
slippa använda deras tjänster. I Fripost har vi en grupp personer som kan detta
bra och bygger system som alla medlemmar tillsammans använder. Vi är över 35
medlemmar som har frigjort vårt e-postande.

Tillsammans har vi resurser och kunskap nog till att steg för steg frigöra vår
datoranvändning. Vad som är omöjligt för någon som är ensam blir enkelt när man
sluter sig samman. Och i Fripost är det medlemmarna som har makten över
tekniken.

Men vi behöver bli fler och Du borde gå med. Fripost behöver dig. Du behöver
Fripost.

Gå in på vår hemsida för mer information: www.fripost.org
